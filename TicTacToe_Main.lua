

local _, TTT = ...

TTT.GAME_MODE = 'MEDIUM'
TTT.OPPONENT = nil
TTT.SELECTED_THEME = 'Basic'
TTT.MoveTimerBarActive = false
TTT.MoveTimerStartTime = nil
TTT.MoveTimerEndTime = nil
TTT.LookingForQuickGame = false
TTT.LookingForQuickGameOpponents = {}
TTT.MoveTimeLimit = 20.0

SLASH_TICTACTOE1 = '/ttt'
SlashCmdList['TICTACTOE'] = function(msg)

	if msg == '-help' then
		print('help')

	elseif msg == 'gamemode-medium' then
		TTT.GAME_MODE = 'MEDIUM'

	elseif msg == 'reset' then
		TTT.PlayAI_Button:Enable()
		for k, v in ipairs(TTT.BoardTiles) do
			v.value = nil
			v:Hide()
			v:Show()
		end

	elseif msg == 'open' then
		TTT.MainFrame:Show()

	end

end

function TTT.RgbToPercent(t)
    if type(t) == 'table' then
        if type(t[1]) == 'number' and type(t[2]) == 'number' and type(t[3]) == 'number' then
            local r = tonumber(t[1] / 256.0)
            local g = tonumber(t[2] / 256.0)
            local b = tonumber(t[3] / 256.0)
            return {r, g, b}
        end
    end
end

TTT.WinTable = {
	{ x = 1, y = 2, z = 3 },
	{ x = 4, y = 5, z = 6 },
	{ x = 7, y = 8, z = 9 },
	{ x = 1, y = 4, z = 7 },
	{ x = 2, y = 5, z = 8 },
	{ x = 3, y = 6, z = 9 },
	{ x = 1, y = 5, z = 9 },
	{ x = 3, y = 5, z = 7 },
}

function TTT.AICanBlock(p)
	local tile = nil
	for k, v in ipairs(TTT.WinTable) do
		local c, e = 0, 0
		for j, l in pairs(v) do
			if tonumber(TTT.BoardTiles[l].value) == tonumber(p) then
				c = c + 1
			elseif tonumber(TTT.BoardTiles[l].value) ~= tonumber(p) and TTT.BoardTiles[l].value ~= nil then
				c = c - 1
			elseif TTT.BoardTiles[l].value == nil then
				tile = l
				e = e + 1
			end
		end
		if c == 2 then
			print('checked line', k, 'player tiles', c, 'empty tiles', e, 'empty tile selected', tile)
			return true, tile
		end
	end
	return false, nil
end

function TTT.AICanWin(p)
	for k, v in ipairs(TTT.WinTable) do
		local c, e = 0, 0
		local tile = nil
		for j, l in pairs(v) do
			if tonumber(TTT.BoardTiles[l].value) == tonumber(p) then
				c = c + 1
			elseif tonumber(TTT.BoardTiles[l].value) ~= tonumber(p) and TTT.BoardTiles[l].value ~= nil then
				c = c - 1
			elseif TTT.BoardTiles[l].value == nil then
				tile = l
				e = e + 1
			end
		end
		if c == 2 then
			print('checked line', k, 'ai tiles', c, 'empty tiles', e, 'empty tile selected', tile)
			return true, tile
		end
	end
	return false, nil
end

function TTT.IsTileEmpty(tile, board)
	if board[tile].value == nil then
		return true
	else
		return false
	end
end

function TTT.AIMakeMoveHardMode(moves)
	local played = false
	local w, t = TTT.AICanWin(1)
	if w == true and t ~= nil then
		if TTT.IsTileEmpty(t, TTT.BoardTiles) == true then
			TTT.HandleMove('AI', t)
			print('ai played tile', t)
			played = true
			return
		end
	end
	local b, t = TTT.AICanBlock(0)
	if b == true and t ~= nil then
		if TTT.IsTileEmpty(t, TTT.BoardTiles) == true then --tile should always be empty as checked by AICanBlock()
			TTT.HandleMove('AI', t)
			print('ai played tile', t)
			played = true
			return
		end
	else
		if type(moves) == 'table' then
			print('no block or win for ai using moves table')
			for k, v in ipairs(moves) do
				if TTT.IsTileEmpty(tonumber(v), TTT.BoardTiles) == true then
					TTT.HandleMove('AI', tonumber(v))
					print('ai played tile', v)
					played = true
					return
				end
			end
		end
	end
	if played == false then
		print('ai playing random tile')
		TTT.HandleMove('AI', TTT.FindRandomEmptyTile())
	end
end

function TTT.WinCheck(p)
	for k, v in ipairs(TTT.WinTable) do
		if tonumber(TTT.BoardTiles[v.x].value) == tonumber(p) and
		tonumber(TTT.BoardTiles[v.y].value) == tonumber(p) and
		tonumber(TTT.BoardTiles[v.z].value) == tonumber(p) then
			print('winner', p)
			TTT.DisableBoard() --stop input
			TTT.MoveTimerBar:Hide()TTT.OPPONENT = nil --stops any further moves being handled
			return true
		end
	end
end

function TTT.DisableBoard()
	for k, v in ipairs(TTT.BoardTiles) do
		v:EnableMouse(false)
	end
end

function TTT.EnableBoard()
	for k, v in ipairs(TTT.BoardTiles) do
		v:EnableMouse(true)
	end
end

function TTT.ResetBoard()
	TTT.PlayAI_Button:Enable()
	for k, v in ipairs(TTT.BoardTiles) do
		v.value = nil
		v:Hide()
		v:Show()
	end
	TTT.DisableBoard()
	TTT.MoveTimerBar:Hide()TTT.OPPONENT = nil
end

TTT.EventFrame = CreateFrame('FRAME', 'TTT_EVENT_FRAME', UIParent)
TTT.EventFrame:RegisterEvent('ADDON_LOADED')
TTT.EventFrame:RegisterEvent('CHAT_MSG_ADDON')

TTT.MainFrame = CreateFrame('FRAME', 'TTT_MAIN_FRAME', UIParent, "UIPanelDialogTemplate")
TTT.MainFrame:SetPoint('CENTER', 0, 0)
TTT.MainFrame:SetSize(400, 400)

--come back to this in time
TTT.PlayAI_Button = CreateFrame('BUTTON', '$parent_QuickGameButton', TTT.MainFrame, "UIPanelButtonTemplate")
TTT.PlayAI_Button:SetSize(100, 22)
TTT.PlayAI_Button:SetText('Play AI')
TTT.PlayAI_Button:SetPoint('BOTTOM', TTT.MainFrame, 'BOTTOM', 0, 15)
TTT.PlayAI_Button:SetScript('OnClick', function(self)
	self:Disable()
	TTT.OPPONENT = 'AI'
	TTT.EnableBoard()
	print('playing AI - good luck!')
end)


TTT.ResetGameButton = CreateFrame('BUTTON', '$parent_ResetGameButton', TTT.MainFrame, "UIPanelButtonTemplate")
TTT.ResetGameButton:SetSize(100, 22)
TTT.ResetGameButton:SetText('Reset Game')
TTT.ResetGameButton:SetPoint('LEFT', TTT.PlayAI_Button, 'RIGHT', 10, 0)
TTT.ResetGameButton:SetScript('OnClick', function(self)
	TTT.ResetBoard()
end)

TTT.ChallengePlayerButton = CreateFrame('BUTTON', '$parent_ChallengePlayerButton', TTT.MainFrame, "UIPanelButtonTemplate")
TTT.ChallengePlayerButton:SetSize(100, 22)
TTT.ChallengePlayerButton:SetText('Challenge')
TTT.ChallengePlayerButton:SetPoint('RIGHT', TTT.PlayAI_Button, 'LEFT', -10, 0)
TTT.ChallengePlayerButton:SetScript('OnClick', function(self)

end)

TTT.Board = CreateFrame('FRAME', '$parent_Board', TTT.MainFrame)
TTT.Board:SetPoint('TOP', TTT.MainFrame, 'TOP', 0, -65)
TTT.Board:SetSize(160, 160)

TTT.MoveTimerBar = CreateFrame('StatusBar', '$parent_MoveStatusBar', TTT.MainFrame)
TTT.MoveTimerBar:SetStatusBarTexture("Interface\\TargetingFrame\\UI-StatusBar")
TTT.MoveTimerBar:GetStatusBarTexture():SetHorizTile(false)
TTT.MoveTimerBar:SetMinMaxValues(0, 100)
TTT.MoveTimerBar:SetValue(0)
TTT.MoveTimerBar:SetSize(100, 20)
TTT.MoveTimerBar:SetPoint('TOP', TTT.Board, 'BOTTOM', 0, -15)
TTT.MoveTimerBar:SetStatusBarColor(TTT.Themes[TTT.SELECTED_THEME].MoveTimerBarColour.r, TTT.Themes[TTT.SELECTED_THEME].MoveTimerBarColour.g, TTT.Themes[TTT.SELECTED_THEME].MoveTimerBarColour.b)


function TTT.DrawBoard()
	TTT.Board.GridLeft = TTT.Board:CreateTexture('$parent_Board_GridLeft', 'BACKGROUND')
	TTT.Board.GridLeft:SetPoint('TOPLEFT', TTT.Board, 'TOPLEFT', 50, 0)
	TTT.Board.GridLeft:SetPoint('BOTTOMRIGHT', TTT.Board, 'BOTTOMLEFT', 55, 0)
	TTT.Board.GridLeft:SetColorTexture(1,1,1,1)

	TTT.Board.GridRight = TTT.Board:CreateTexture('$parent_Board_GridRight', 'BACKGROUND')
	TTT.Board.GridRight:SetPoint('TOPLEFT', TTT.Board, 'TOPLEFT', 105, 0)
	TTT.Board.GridRight:SetPoint('BOTTOMRIGHT', TTT.Board, 'BOTTOMLEFT', 110, 0)
	TTT.Board.GridRight:SetColorTexture(1,1,1,1)

	TTT.Board.GridTop = TTT.Board:CreateTexture('$parent_Board_GridTop', 'BACKGROUND')
	TTT.Board.GridTop:SetPoint('TOPLEFT', TTT.Board, 'TOPLEFT', 0, -50)
	TTT.Board.GridTop:SetPoint('BOTTOMRIGHT', TTT.Board, 'TOPRIGHT', 0, -55)
	TTT.Board.GridTop:SetColorTexture(1,1,1,1)

	TTT.Board.GridBottom = TTT.Board:CreateTexture('$parent_Board_GridBottom', 'BACKGROUND')
	TTT.Board.GridBottom:SetPoint('TOPLEFT', TTT.Board, 'TOPLEFT', 0, -105)
	TTT.Board.GridBottom:SetPoint('BOTTOMRIGHT', TTT.Board, 'TOPRIGHT', 0, -110)
	TTT.Board.GridBottom:SetColorTexture(1,1,1,1)

	local tilePos = {
		[1] = { x = 0.0, y = 0.0 },
		[2] = { x = 55.0, y = 0.0 },
		[3] = { x = 110.0, y = 0.0 },
		[4] = { x = 0.0, y = -55.0 },
		[5] = { x = 55.0, y = -55.0 },
		[6] = { x = 110.0, y = -55.0 },
		[7] = { x = 0.0, y = -110.0 },
		[8] = { x = 55.0, y = -110.0 },
		[9] = { x = 110.0, y = -110.0 },
	}

	TTT.BoardTiles = {}
	for i = 1, 9 do
		local f = CreateFrame('FRAME', tostring('$parent_BoardTile_'..i), TTT.Board)
		f:SetSize(50, 50)
		f:SetPoint('TOPLEFT', tilePos[i].x, tilePos[i].y)

		f.Background = f:CreateTexture(nil, 'BACKGROUND', nil, -5)
		f.Background:SetAllPoints(f)
		f.Background:SetColorTexture(1,1,1,0.1)

		f.Piece = f:CreateTexture(nil, 'BACKGROUND')
		f.Piece:SetAllPoints(f)
		f.Piece:Hide()

		f.tile = i
		f.value = nil

		f:SetScript('OnEnter', function(self)
			if not self.value then
				self.Background:SetColorTexture(1,1,1,0.3)
			end
		end)
		f:SetScript('OnLeave', function(self)
			self.Background:SetColorTexture(1,1,1,0.1)
		end)
		f:SetScript('OnMouseDown', function(self)
			TTT.HandleMove(TTT.PLAYER_NAME, self.tile)
		end)
		f:SetScript('OnHide', function(self)
			self.Piece:Hide()
			self.Piece:SetTexture(nil)
		end)
		f:SetScript('OnShow', function(self)
			if self.value then
				self.Piece:SetTexture(tonumber(TTT.Themes[TTT.SELECTED_THEME].PieceTexture[self.value]))
				self.Piece:Show()
			end		
		end)
		TTT.BoardTiles[i] = f
	end
end

TTT.AI = {
	EASY = {
		[1] = function() TTT.HandleMove('AI', TTT.FindRandomEmptyTile()) end,
		[2] = function() TTT.HandleMove('AI', TTT.FindRandomEmptyTile()) end,
		[3] = function() TTT.HandleMove('AI', TTT.FindRandomEmptyTile()) end,
		[4] = function() TTT.HandleMove('AI', TTT.FindRandomEmptyTile()) end,
		[5] = function() TTT.HandleMove('AI', TTT.FindRandomEmptyTile()) end,
		[6] = function() TTT.HandleMove('AI', TTT.FindRandomEmptyTile()) end,
		[7] = function() TTT.HandleMove('AI', TTT.FindRandomEmptyTile()) end,
		[8] = function() TTT.HandleMove('AI', TTT.FindRandomEmptyTile()) end,
		[9] = function() TTT.HandleMove('AI', TTT.FindRandomEmptyTile()) end,
	},
	MEDIUM = {
		[1] = function()
			local r = math.random(1, 100)
			if r < 34 then
				TTT.AIMakeMoveHardMode({5, 3, 7, 9})
			elseif r > 66 then
				TTT.AIMakeMoveHardMode({5, 7, 9, 3})
			else
				TTT.AIMakeMoveHardMode({5, 9, 7, 3})
			end
		end,
		[2] = function()
			local r = math.random(1, 100)
			if r < 50 then
				TTT.AIMakeMoveHardMode({1, 3, 5, 7, 9})
			else
				TTT.AIMakeMoveHardMode({3, 1, 5, 7, 9})
			end
		end,
		[3] = function()
			local r = math.random(1, 100)
			if r < 34 then
				TTT.AIMakeMoveHardMode({5, 1, 7, 9})
			elseif r > 66 then
				TTT.AIMakeMoveHardMode({5, 7, 9, 1})
			else
				TTT.AIMakeMoveHardMode({5, 9, 7, 1})
			end
		end,
		[4] = function()
			local r = math.random(1, 100)
			if r < 50 then
				TTT.AIMakeMoveHardMode({1, 8, 5, 3, 9})
			else
				TTT.AIMakeMoveHardMode({8, 1, 5, 3, 9})
			end
		end,
		[5] = function()
			local r = math.random(1, 100)
			if r < 25 then
				TTT.AIMakeMoveHardMode({1, 3, 7, 9})
			elseif r > 24 and r < 50 then
				TTT.AIMakeMoveHardMode({7, 9, 1, 3})
			elseif r > 49 and r < 75 then
				TTT.AIMakeMoveHardMode({3, 7, 9, 1})
			else
				TTT.AIMakeMoveHardMode({9, 1, 3, 7})
			end
		end,
		[6] = function()
			local r = math.random(1, 100)
			if r < 50 then
				TTT.AIMakeMoveHardMode({9, 3, 5, 1, 8})
			else
				TTT.AIMakeMoveHardMode({3, 9, 5, 1, 8})
			end
		end,
		[7] = function()
			local r = math.random(1, 100)
			if r < 34 then
				TTT.AIMakeMoveHardMode({5, 1, 3, 9})
			elseif r > 66 then
				TTT.AIMakeMoveHardMode({5, 3, 9, 1})
			else
				TTT.AIMakeMoveHardMode({5, 9, 3, 1})
			end
		end,
		[8] = function()
			local r = math.random(1, 100)
			if r < 50 then
				TTT.AIMakeMoveHardMode({9, 7, 5, 1, 3})
			else
				TTT.AIMakeMoveHardMode({7, 9, 5, 1, 3})
			end
		end,
		[9] = function()
			local r = math.random(1, 100)
			if r < 34 then
				TTT.AIMakeMoveHardMode({5, 1, 3, 7})
			elseif r > 66 then
				TTT.AIMakeMoveHardMode({5, 3, 7, 1})
			else
				TTT.AIMakeMoveHardMode({5, 7, 3, 1})
			end
		end,
	}
}

function TTT.FindRandomEmptyTile()
	for i = 1, 9 do
		local r = math.random(1, 9)
		if TTT.BoardTiles[r].value == nil then
			print('tile:', r, 'empty')
			return r
		end
		if i == 9 then
			return -1
		end
	end
end

function TTT.HandleMove(player, move)
	print('handling move from', player, move)
	if tostring(player) == tostring(TTT.OPPONENT) and TTT.OPPONENT ~= 'AI' then
		print('opponents move', tonumber(move))
		TTT.BoardTiles[tonumber(move)].value = 1
		TTT.BoardTiles[tonumber(move)]:Hide()
		TTT.BoardTiles[tonumber(move)]:Show()
		--after receiving opponents move set move timer bar to update
		TTT.MoveTimerBarActive = true
		TTT.MoveTimerStartTime = GetTime()
		TTT.MoveTimerEndTime = GetTime() + TTT.MoveTimeLimit
		TTT.WinCheck(0) -- 0 is always the clients player, check them first
		TTT.WinCheck(1)
		C_Timer.After(TTT.MoveTimeLimit, function() --needed?
			TTT.MoveTimerBarActive = false
		end)
	elseif tostring(player) == tostring(TTT.PLAYER_NAME) then
		print('my move', tonumber(move))
		TTT.BoardTiles[tonumber(move)].value = 0 --set tile to my piece
		TTT.BoardTiles[tonumber(move)]:Hide()
		TTT.BoardTiles[tonumber(move)]:Show()
		TTT.WinCheck(0) -- 0 is always the clients player, check them first
		TTT.WinCheck(1)
		if TTT.OPPONENT == 'AI' then
			C_Timer.After(1.5, function()
				if TTT.OPPONENT == 'AI' then --check game wasnt reset
					TTT.AI[TTT.GAME_MODE][tonumber(move)]()
				end
			end)
		end
	elseif tostring(player) == 'AI' then
		if tonumber(move) == -1 then --no move available to AI
			print('no more empty tiles!')
			TTT.OPPONENT = nil --stop game
			TTT.DisableBoard()
			return 
		end 
		TTT.BoardTiles[tonumber(move)].value = 1
		TTT.BoardTiles[tonumber(move)]:Hide()
		TTT.BoardTiles[tonumber(move)]:Show()
		TTT.WinCheck(0) -- 0 is always the clients player, check them first
		TTT.WinCheck(1)
	end
end

function TTT.Init()

	TTT.PLAYER_NAME = select(1, UnitName('player'))

	local regChatPrefixMove = C_ChatInfo.RegisterAddonMessagePrefix('ttt-move')
	if regChatPrefixMove then
		print('registered chat prefix for moves')
	end
	local regChatPrefixFind = C_ChatInfo.RegisterAddonMessagePrefix('ttt-find')
	if regChatPrefixFind then
		print('registered chat prefix for finding games')
	end

	TTT.DrawBoard()

	-- if not TTT_GLOBAL then
	-- 	TTT_GLOBAL = {
	-- 		AddonName = 'TicTaToe',
	-- 	}
	-- end
	-- if not TTT_CHARACTER then
	-- 	TTT_CHARACTER = {
	-- 		ShowMinimapButton = true,
	-- 		MinimapButton = {},
	-- 	}
	-- end

	print('loaded tic tac toe')
end

TTT.Events = {
	['ADDON_LOADED'] = function(self, ...)
		if select(1, ...):lower() == 'tictactoe' then
			TTT.Init()
		end
	end,
	['CHAT_MSG_ADDON'] = function(self, ...)
		local prefix = select(1, ...)
		local msg = select(2, ...)
		local sender = select(4, ...)

		if string.find(prefix, 'ttt-') then
			--print(prefix, msg, sender)
		end

		--lfg - lfqg received=reply afqg, afqg received=add name to list
		if prefix == 'ttt-find' then
			if msg == 'lfqg' and sender ~= TTT.PLAYER_NAME then
				if TTT.LookingForQuickGame == true then
					C_ChatInfo.SendAddonMessage('ttt-find', 'afqg', 'WHISPER', sender)
				end
			end
			if msg == 'afqg' then
				TTT.LookingForQuickGameOpponents[sender] = true
			end
		end

		if prefix == 'ttt-move' then
			TTT.HandleMove(sender, msg)
		end
	end,
}

function TTT.OnEvent(self, event, ...)
	TTT.Events[event](TTT, ...)
end

function TTT:OnUpdate()
	if self.MoveTimerBarActive == true then
		if GetTime() > TTT.MoveTimerEndTime then return end
		self.MoveTimerBar:SetValue(tonumber((TTT.MoveTimerEndTime - GetTime()) / TTT.MoveTimeLimit) * 100)
	end
end

TTT.EventFrame:SetScript("OnEvent", TTT.OnEvent)
TTT.EventFrame:SetScript("OnUpdate", function() TTT:OnUpdate() end)


--[==[
	USED TO FIND PLAYERS - MOVING TO LOBBY IDEA
	local msg = tostring('tictactoe-quickgame')
	local quickGameFinder = C_ChatInfo.SendAddonMessage('ttt-find', 'lfqg', 'YELL') --use yell for largest pool of players likely to have addon
	if quickGameFinder then
		print('looking for quick game!')
		self:Disable()
		TTT.LookingForQuickGame = true
		TTT.LookingForQuickGameOpponents = {}
		C_Timer.After(10, function() 
			TTT.LookingForQuickGame = false --no longer accept players to list
			--pick random player from list and try to make connection for game
			--TTT.PlayAI_Button:Enable()
		end)
	end
]==]--