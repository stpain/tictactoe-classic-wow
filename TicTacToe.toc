## Interface: 11300
## Author: Copperbolts
## Version: 0.0.1
## Title: |cff0070DE TicTacToe |r
## Notes: 
## SavedVariables: TTT_GLOBAL
## SavedVariablesPerCharacter: TTT_CHARACTER

Libs\LibStub\LibStub.lua
Libs\CallbackHandler-1.0\CallbackHandler-1.0.lua
Libs\LibDataBroker-1.1\LibDataBroker-1.1.lua
Libs\LibDbIcon-1.0\LibDbIcon-1.0.lua

TicTacToe_Data.lua
TicTacToe_Main.lua