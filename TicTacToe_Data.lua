

local _, TTT = ...

TTT.Themes = {
	Basic = {
		PieceTexture = {
			[0] = 132048,
			[1] = 130775,
		},
		MoveTimerBarColour = {
			r = 1.0,
			g = 1.0,
			b = 1.0,
		}
	}
}